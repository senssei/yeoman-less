'use strict';

/**
 * @ngdoc function
 * @name yeomanLessApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the yeomanLessApp
 */
angular.module('yeomanLessApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
