'use strict';

/**
 * @ngdoc function
 * @name yeomanLessApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yeomanLessApp
 */
angular.module('yeomanLessApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
